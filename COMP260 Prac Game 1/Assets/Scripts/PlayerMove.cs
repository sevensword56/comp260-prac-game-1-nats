﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float maxSpeed = 10.0f; // in metres per second
    public float acceleration = 5.0f; // in metres/second/second
    public float brake = 6.0f; // in metres/second/second
    public float turnSpeed = 200.0f; // in degrees/second
    public float turnRate = 5.0f;
    public float destroyRadius = 1.0f;
    public string horizontalAxis;
    public string verticalAxis;
    public string fireButton;
    private float speed = 0.0f;
    private BeeSpawner beeSpawner;

    // Use this for initialization
    void Start () {
        
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();

    }
	
	// Update is called once per frame
	void Update () {

        float turnSpeedActual = turnSpeed;

        // the horizontal axis controls the turn
        float turn = Input.GetAxis(horizontalAxis) * -1;
        // turn the car
        if (speed > 0)
            turnSpeedActual = turnSpeed - turnRate * speed;
        else if (speed < 0)
        {
            turnSpeedActual = turnSpeed - turnRate * (speed * -1);
            turn = Input.GetAxis(horizontalAxis);
        }

        transform.Rotate(0, 0, (turn * turnSpeedActual) * Time.deltaTime);


        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(verticalAxis);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
            }
            else if (speed < 0)
            {
                speed = speed + brake * Time.deltaTime;
            }

            if ((speed > 0 && speed < 1) || (speed < 0 && speed > -1))
                speed = 0;
            
        }

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);

        //Too Destroy Bees
        if (Input.GetButtonDown(fireButton))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }


    }
}
