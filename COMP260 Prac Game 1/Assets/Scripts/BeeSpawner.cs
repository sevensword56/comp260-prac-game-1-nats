﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public int nBees = 50;
    public Rect spawnRect;
    public BeeMove beePrefab;
    public PlayerMove player;
    public PlayerMove player2;
    public float beePeriod = 0.0f;
    public float minBeePeriod = 0.0f;
    public float maxBeePeriod = 0.0f;
    private float timer = 0.0f;
    private float timer2 = 0.0f;
    private float randBee = 0.0f;
    private int beeNumb = 0;
    private bool beePeriodSelected = false;
    
    



    // Use this for initialization
    void Start () {
        // create bees
        for (int i = 0; i < nBees; i++)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);
            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);

            // set the target
            bee.target = player.transform;
            bee.target2 = player2.transform;

            beeNumb = i;
        }


    }

    // Update is called once per frame
    void Update () {

        timer += Time.deltaTime;
        timer2 += Time.deltaTime;

        if(beePeriodSelected == false)
        {
            randBee = Random.Range(minBeePeriod, maxBeePeriod);
            beePeriodSelected = true;
        }

        if( timer >= beePeriod || timer2 >= randBee)
        {
            beeNumb += 1;

            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);
            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + beeNumb;

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);

            // set the target
            bee.target = player.transform;
            bee.target2 = player2.transform;

            if (timer >= randBee)
            {
                beePeriodSelected = false;
                timer2 = 0;
            }
            else
                timer = 0;
            
        }


		
	}

    public void DestroyBees (Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

    


    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
}
